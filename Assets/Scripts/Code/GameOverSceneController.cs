﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverSceneController : SceneController<GameOverSceneController>
{
    public static bool IsGameLost;
    public Text GameOverText;
    // Use this for initialization
    void Start()
    {
        if (IsGameLost)
        {
            GameOverText.text = @"GAME OVER!! \n Press X to continue".Replace("\\n", "\n");
        }
        else
        {
            GameOverText.text = string.Format("You won!!! \n Score earned : {0}  \n Press X to continue.", GameController.Score).Replace("\\n", "\n");
        }
    }

    private void Update()
    {
        AwaitKey();
 
    }

    // Update is called once per frame
    private void AwaitKey()
    {
        var xKey = Input.GetKey(KeyCode.X);
        if (xKey)
        {
            SceneManager.LoadScene(0);
        }
    }
}
