﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController<T> : MonoBehaviour where T : SceneController<T>
{
    static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null) _instance = (T)FindObjectOfType(typeof(T));
            if (_instance == null) Debug.LogError("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
            return _instance;
        }
    }

    protected void Awake()
    {
        if (_instance == null) _instance = this as T;
        else if (_instance != this) Destroy(this);
    }
}
