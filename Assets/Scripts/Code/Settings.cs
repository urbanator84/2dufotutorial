﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Code
{
    class Settings : MonoBehaviour
    {
        private  float _difficultyLevel;
        public Text Label;
        

        public void Start()
        {
            _difficultyLevel = 0;
            Label.text = _difficultyLevel.ToString();
        }

        public void Update()
        {
            //DifficultyLevel.DifficultyValue = _difficultyLevel;
            //Label.text = _difficultyLevel.ToString();
        }

        public void SetSliderValue(float sliderValue)
        {
            DifficultyLevel.DifficultyValue = Mathf.Round(sliderValue);
            Label.text = Mathf.Round(sliderValue).ToString();
        }
    }
}
