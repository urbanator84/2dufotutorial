﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Code
{
    class DifficultyLevel : ScriptableObject
    {
        private static float _difficultyValue;
        public static float TimeMultiplier { get; private set; }
        public static float SizeMultiplier { get; private set; }
        
        public static float DifficultyValue
        {
            get
            {
                return _difficultyValue;
            }

            set
            {
                _difficultyValue = value;
            }
        }
        private void Awake()
        {
            TimeMultiplier /= _difficultyValue;
            SizeMultiplier *= _difficultyValue;
        }
    }
}
