﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Scripts.Constants;
using Assets.Scripts.Actions;
using Assets.Scripts.Code;

public class GameController : SceneController<GameController>
{
    
    public Text CountText;
    public Text WinText;
    public Text TimeCounter;
    public Text DiffLevel;
    public GameObject PickUp;
    public GameObject Board;

    private GameObject _player;
    private float _targetTime;
    private static int _score;
    private GameObject[] _pickups;
    private SpawnObjectsAction _spawner;

    void Start ()
    {
        _score = 0;
        Time.timeScale = 1;
        _targetTime = 30;
        _player = GameObject.FindGameObjectsWithTag(Names.PLAYER_TAG)[0];
        _player.SetActive(true);

        var boardSize = Board.transform;
        WinText.text = string.Empty;
        CountText.text = string.Format("Pickups collected: {0}", _score);
        DiffLevel.text = string.Format("Difficulty : {0} points", DifficultyLevel.DifficultyValue.ToString());
        _spawner = ScriptableObject.CreateInstance(typeof(SpawnObjectsAction)) as SpawnObjectsAction;
        _spawner.Initialization(5+((int)DifficultyLevel.DifficultyValue *2));
        _spawner.SpawnPickups();  
    }
	void FixedUpdate ()
    {
        _targetTime -= Time.deltaTime;
        TimeCounter.text = Mathf.Round(_targetTime).ToString();
        
    }

    void Update()
    {
        _pickups = GameObject.FindGameObjectsWithTag(Names.PICKUP_TAG);
        CountText.text = string.Format("Pickups collected: {0}", Score);
        GameOver();
    }
    public static int Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
        }
    }
    #region Private Methods

    private void GameOver ()
    {
        if (_targetTime <= 0)
        {
            GameOverSceneController.IsGameLost = true;
        }
        else if (_targetTime > 0 && _pickups.Length == 0)
        {
            GameOverSceneController.IsGameLost = false;
        }
        else
        {
            return;
        }
        _targetTime = 0;
        SceneManager.LoadScene(Names.GAMEOVER_SCENE_NAME);
    }
   

    #endregion

}
