﻿using UnityEngine.SceneManagement;

namespace Assets.Scripts.Actions
{
    class StartGameButton : BaseButtonActions
    {
        public override void ClickAction()
        {
            SceneManager.LoadScene(Constants.Names.MAIN_SCENE_NAME);
        }
    }
}
