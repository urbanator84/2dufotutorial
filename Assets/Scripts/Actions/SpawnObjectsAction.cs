﻿using UnityEngine;

namespace Assets.Scripts.Actions
{
    class SpawnObjectsAction : ScriptableObject
    {
        private GameObject _prefab;
        private Transform _boardSize;
        private int _spawnsNumber;

        public void Initialization (int spawnsNumber)
        {
            _prefab = GameController.Instance.PickUp;
            _boardSize = GameController.Instance.Board.transform;
            _spawnsNumber = spawnsNumber;
        }

        public void SpawnPickups ()
        {
            for (var n = 0; n<= _spawnsNumber; n++)
            {
                SpawnPrefab();
            }
        }

        private void SpawnPrefab ()
        {
            var newPosition = NewPosition();
            CircleCollider2D prefabCollider = _prefab.GetComponent<CircleCollider2D>();
            if (!prefabCollider)
            {
                Debug.LogError(string.Format("No collider found for {0}", _prefab.name));
                throw new System.Exception(string.Format("No collider found for {0}", _prefab.name));
            }

            if (Physics2D.OverlapCircle(newPosition, prefabCollider.radius)== null)
            {
               Instantiate(_prefab, newPosition, Quaternion.identity);
            }
            else
            {
                SpawnPrefab();
            }

        }
        private Vector3 NewPosition()
        {
            float x = Random.Range(-_boardSize.GetComponent<Renderer>().bounds.extents.x, _boardSize.GetComponent<Renderer>().bounds.extents.x);
            float y = UnityEngine.Random.Range(-_boardSize.GetComponent<Renderer>().bounds.extents.y, _boardSize.GetComponent<Renderer>().bounds.extents.y);

            return new Vector3(_prefab.transform.position.x + x, _prefab.transform.position.y + y, _prefab.transform.position.z);
        }
    }
}
