﻿using UnityEngine;
using Assets.Scripts.Constants;

namespace Assets.Scripts.Actions
{
    public class MovementAction
    {
        public static void MoveObjectManually (Rigidbody2D rigidBody, float speed)
        {
            float moveHorizontal = Input.GetAxis(Names.X_AXIS);
            float moveVertical = Input.GetAxis(Names.Y_AXIS);
            Vector2 movement = new Vector2(moveHorizontal, moveVertical);

            rigidBody.AddForce(movement * speed);
        }

        public static void MoveObjectRandomly (Rigidbody2D rigidBody, float speed)
        {
            
        }
    }
}
