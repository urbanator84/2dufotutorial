﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Actions
{
    
    class BaseButtonActions : MonoBehaviour
    {
        public Button ActionButton;

        void Start ()
        {
            Button button = ActionButton.GetComponent<Button>();
            button.onClick.AddListener(ClickAction);
        }

        public virtual void ClickAction()
        {
            throw new System.Exception("The Click action is not implemented!!!");
        }
    }
}
