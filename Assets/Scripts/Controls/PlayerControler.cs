﻿using UnityEngine;
using Assets.Scripts.Constants;
using Assets.Scripts.Actions;

public class PlayerControler : MonoBehaviour
{
    private float _speed;
    private Rigidbody2D _rigidBody;
	void Start ()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _speed = 20;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void FixedUpdate()
    {
        MovementAction.MoveObjectManually(_rigidBody, _speed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(Names.PICKUP_TAG))
        {
            other.gameObject.SetActive(false);
            GameController.Score++;
           
        }
    }

    public static void DestroyPlayer(GameObject player)
    {
        if (player != null)
        {
            player.gameObject.SetActive(false);
        }

    }
}
