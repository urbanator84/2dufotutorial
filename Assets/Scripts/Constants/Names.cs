﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Constants
{
    class Names
    {
        public const string PICKUP_TAG = "PickUp";
        public const string X_AXIS = "Horizontal";
        public const string Y_AXIS = "Vertical";
        public const string MAIN_SCENE_NAME = "Main";
        public const string PLAYER_TAG= "Player";
        public const string GAMEOVER_SCENE_NAME = "GameOver";
    }
}
